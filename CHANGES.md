# ChangeLog
## version 3.2.0 - 2025-01-09
- switch to qt 6.8
## version 3.1.0 - 2023-11-09
- update cmakelists
- switch to c++20
## version 3.0.0 - 2023-09-21
- update to Qt6
- only need min and max from std for gdi headers + use c++17
## version 2.2.0 - 2022-05-23
 - make setcentralwidget virtual
 - default theme is Nord
## version 2.1.17 - 2020-05-13
 - styling for QMessageBox
## version 2.1.16 - 2020-04-22
 - Fix font size of QListView
## version 2.1.15 - skipped
## version 2.1.13 - 2020-03-17
 - Adding new themes
## version 2.1.12 - 2020-03-15
 - Fixing version numbers
## version 2.1.11 - 2020-03-15
 - Add properties bindings
## version 2.1.11 - 2020-02-26
 - Revert callback handling
## version 2.1.10 - 2020-02-26
 - Fix bug on dynamic switch
## version 2.1.9 - 2020-02-26
 - Adding callback framework
## version 2.1.8 - 2020-01-14
 - Fix main window on mac and windows
