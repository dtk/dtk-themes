// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtDebug>
#include <QtWidgets>

#include <dtkFonts>
#include <dtkThemes>
#include <dtkThemesWidgets>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dummyWindow : public dtkThemesWidgetsMainWindow
{
    Q_OBJECT

public:
     dummyWindow(QWidget *parent = nullptr);
    ~dummyWindow(void);

public:
    dtkThemesWidget *widget;
};

dummyWindow::dummyWindow(QWidget *parent) : dtkThemesWidgetsMainWindow(parent)
{
    QSettings settings;

    QComboBox *chooser = new QComboBox(this);
    chooser->addItems(dtkThemesEngine::instance()->themes());
    chooser->setCurrentText(settings.value("theme").toString());

    QObject::connect(chooser, SIGNAL(currentTextChanged(const QString&)), dtkThemesEngine::instance(), SLOT(apply(const QString&)));

    this->widget = new dtkThemesWidget(this);

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    QHBoxLayout *tbl = new QHBoxLayout;
    tbl->addWidget(new QCheckBox("CheckBox", this));
    tbl->addWidget(new QRadioButton("Radio Button", this));

    QGroupBox *boxl = new QGroupBox("QGroupBox", this); boxl->setLayout(new QVBoxLayout);
    QGroupBox *boxr = new QGroupBox("QGroupBox", this); boxr->setLayout(new QVBoxLayout);

    boxl->layout()->addWidget(new QSpinBox(this));
    boxl->layout()->addWidget(new QDoubleSpinBox(this));

    boxr->layout()->addWidget(new QSpinBox(this));
    boxr->layout()->addWidget(new QDoubleSpinBox(this));

    QHBoxLayout *mbl = new QHBoxLayout;
    mbl->addWidget(boxl);
    mbl->addWidget(boxr);

    QVBoxLayout *inner = new QVBoxLayout;
    inner->setContentsMargins(10, 10, 10, 10);
    inner->setSpacing(10);
    inner->addSpacing(25);
    inner->addWidget(new QPushButton("QPushButton", this));
    inner->addLayout(tbl);
    inner->addWidget(new QLineEdit("QLineEdit", this));
    inner->addLayout(mbl);
    inner->addStretch();
    inner->addWidget(chooser);

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addLayout(inner);
    layout->addWidget(this->widget);

    QFrame *central = new QFrame(this);
    central->setLayout(layout);

    this->setCentralWidget(central);
}

dummyWindow::~dummyWindow(void)
{

}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    QSettings::setDefaultFormat(QSettings::IniFormat);

    QApplication application(argc, argv);
    application.setApplicationName("dtkThemesEngine");
    application.setApplicationVersion("0.1");
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");

    dtkThemesEngine::instance()->apply();


    dummyWindow *window = new dummyWindow;
    window->setWindowTitle("dtkThemesEngine");
    window->resize(800, 600);
    window->show();
    window->raise();

    dtkThemesEngine::instance()->apply();  // Check if still useful when all .qless stylesheets are ok.

    return application.exec();
}

// /////////////////////////////////////////////////////////////////////////////

#include "main.moc"

//
// main.cpp ends here
