// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtDebug>

#include <dtkThemes/dtkThemesParser>

int main(int argc, char **argv)
{
    QCoreApplication application(argc, argv);
    application.setApplicationName("dtkThemesParser");
    application.setApplicationVersion("0.1");

    QCommandLineOption verboseOption(QStringList() << "v" << "verbose", "Print retrieved <key, value> hash.");

    QCommandLineParser parser;
    parser.setApplicationDescription("dtk stylesheet parser that converts .qless into .qss");
    parser.addHelpOption();
    parser.addPositionalArgument("source", QCoreApplication::translate("main", "Source file to convert."));
    parser.addOption(verboseOption);
    parser.process(application);

    const QStringList arguments = parser.positionalArguments();

    if (arguments.count() < 1) {
        parser.showHelp();
        return 0;
    }

    dtkThemesParser theme_parser;

    qDebug() << theme_parser.parse(arguments.first()).toStdString().c_str();

    if (parser.isSet(verboseOption)) {
        qDebug() << "";
        qDebug() << "Retrieved variables:";
        qDebug() << "";
        qDebug() << theme_parser.values();
    }

    return 0;
}

//
// main.cpp ends here
