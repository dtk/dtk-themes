// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkTheme.h"
#include "dtkThemesEngine.h"
#include "dtkThemesParser.h"

#include <QtWidgets>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

dtkThemesEngineCallBack::dtkThemesEngineCallBack(void) : QObject()
{
    dtkThemesEngine::instance()->addCallBack(this);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkThemesEnginePrivate
{
public:
    dtkTheme *theme = nullptr;
    dtkThemesParser *parser = nullptr;

public:
    QString chosen;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkThemesEngine *dtkThemesEngine::instance(void)
{
    if(!s_instance)
        s_instance = new dtkThemesEngine;

    return s_instance;
}

void displayDirContent(QDir& dir)
{
//    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
//    dir.setSorting(QDir::Size | QDir::Reversed);

    QFileInfoList list = dir.entryInfoList();
    qDebug() << "     Bytes Filename\n";
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        qDebug() << qPrintable(QString("%1 %2").arg(fileInfo.size(), 10)
                                        .arg(fileInfo.fileName()));
        qDebug() << "\n";
    }
}
const QStringList dtkThemesEngine::themes(void)
{
    QDir ressources(":dtk-themes/mixins");

    return ressources.entryList(QStringList() << "*.qless").replaceInStrings("dtkTheme", "").replaceInStrings(".qless", "");
}

void dtkThemesEngine::apply(const QString& theme)
{
    if(!d->parser)
        d->parser = new dtkThemesParser(this);

    d->chosen = theme;

    if (d->chosen.isEmpty()) {

        QSettings settings;

        auto theme_list = this->themes();
        auto id = theme_list.indexOf("Nord");
        auto theme = theme_list.at(id);

        d->chosen = settings.value("theme", theme).toString();
    }

    QString sheet = d->parser->parse(QString(":dtk-themes/mixins/dtkTheme%1.qless").arg(d->chosen));

    // TODO: @Nico: You're right but that is not the right fix

    // if (d->theme) {
    //     d->theme->deleteLater();
    // }

    d->theme = new dtkTheme(sheet);

    qApp->setStyle(d->theme);

    {
        QSettings settings;

        settings.setValue("theme", d->chosen);
    }

    emit changed();
}

void dtkThemesEngine::polish(QWidget *w)
{
    if (!d->theme)
        return;

    d->theme->touch(w);
}

Q_INVOKABLE const QColor dtkThemesEngine::color(const QString& key) const
{
    return d->parser->color(key);
}

Q_INVOKABLE const QString dtkThemesEngine::value(const QString& key) const
{
    return d->parser->values()[key];
}

dtkThemesEngine *dtkThemesEngine::s_instance = nullptr;

dtkThemesEngine::dtkThemesEngine(QObject *parent) : QObject(parent)
{
    d = new dtkThemesEnginePrivate;

    connect(this, &dtkThemesEngine::changed, [=] (void) -> void
    {
        foreach(dtkThemesEngineCallBack *callback, this->callbacks)
            callback->execute();
    });
}

dtkThemesEngine::~dtkThemesEngine(void)
{
    if (d->parser)
        delete d->parser;

    if (d->theme)
        delete d->theme;

    delete d;
}

bool dtkThemesEngine::dark(void)
{
    return !(d->chosen.contains("Light"));
}

//
// dtkThemesEngine.cpp ends here
