// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkThemesExport>

#include <QtCore>

class QWidget;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class DTKTHEMES_EXPORT dtkThemesEngineCallBack : public QObject
{
public:
    dtkThemesEngineCallBack(void);

public:
    virtual void execute(void) = 0;
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class DTKTHEMES_EXPORT dtkThemesEngine : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString base0 READ base0 NOTIFY changed)
    Q_PROPERTY(QString base1 READ base0 NOTIFY changed)
    Q_PROPERTY(QString base2 READ base0 NOTIFY changed)
    Q_PROPERTY(QString base3 READ base0 NOTIFY changed)
    Q_PROPERTY(QString base4 READ base0 NOTIFY changed)
    Q_PROPERTY(QString base5 READ base0 NOTIFY changed)
    Q_PROPERTY(QString base6 READ base0 NOTIFY changed)
    Q_PROPERTY(QString base7 READ base0 NOTIFY changed)
    Q_PROPERTY(QString base8 READ base0 NOTIFY changed)

    Q_PROPERTY(QString fg    READ fg    NOTIFY changed)
    Q_PROPERTY(QString fgalt READ fgalt NOTIFY changed)
    Q_PROPERTY(QString bg    READ bg    NOTIFY changed)
    Q_PROPERTY(QString bgalt READ bgalt NOTIFY changed)
    Q_PROPERTY(QString hl    READ hl    NOTIFY changed)
    Q_PROPERTY(QString hlalt READ hlalt NOTIFY changed)
    Q_PROPERTY(QString bd    READ bd    NOTIFY changed)
    Q_PROPERTY(QString bdalt READ bdalt NOTIFY changed)

    Q_PROPERTY(QString grey READ grey NOTIFY changed)
    Q_PROPERTY(QString red READ red NOTIFY changed)
    Q_PROPERTY(QString orange READ orange NOTIFY changed)
    Q_PROPERTY(QString green READ green NOTIFY changed)
    Q_PROPERTY(QString teal READ teal NOTIFY changed)
    Q_PROPERTY(QString yellow READ yellow NOTIFY changed)
    Q_PROPERTY(QString blue READ blue NOTIFY changed)
    Q_PROPERTY(QString darkblue READ darkblue NOTIFY changed)
    Q_PROPERTY(QString magenta READ magenta NOTIFY changed)
    Q_PROPERTY(QString violet READ violet NOTIFY changed)
    Q_PROPERTY(QString cyan READ cyan NOTIFY changed)
    Q_PROPERTY(QString darkcyan READ darkcyan NOTIFY changed)

    Q_PROPERTY(bool dark READ dark NOTIFY changed)

public:
    static dtkThemesEngine *instance(void);

signals:
    void changed(void);

public:
    Q_INVOKABLE const QStringList themes(void);

public:
    void addCallBack(dtkThemesEngineCallBack *callback)
    {
        this->callbacks << callback;
    }

public slots:
    void apply(const QString& theme = QString());

public slots:
    void polish(QWidget *);

public:
    Q_INVOKABLE const QColor  color(const QString& key) const;
    Q_INVOKABLE const QString value(const QString& key) const;

protected:
    static dtkThemesEngine *s_instance;

private:
     dtkThemesEngine(QObject *parent = nullptr);
    ~dtkThemesEngine(void);

private:
    QList<dtkThemesEngineCallBack *> callbacks;

private:
    class dtkThemesEnginePrivate *d;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

public slots:
    QString base0(void) { return this->value("@base0"); }
    QString base1(void) { return this->value("@base1"); }
    QString base2(void) { return this->value("@base2"); }
    QString base3(void) { return this->value("@base3"); }
    QString base4(void) { return this->value("@base4"); }
    QString base5(void) { return this->value("@base5"); }
    QString base6(void) { return this->value("@base6"); }
    QString base7(void) { return this->value("@base7"); }
    QString base8(void) { return this->value("@base8"); }

    QString fg   (void) { return this->value("@fg");    }
    QString fgalt(void) { return this->value("@fgalt"); }
    QString bg   (void) { return this->value("@bg");    }
    QString bgalt(void) { return this->value("@bgalt"); }
    QString hl   (void) { return this->value("@hl");    }
    QString hlalt(void) { return this->value("@hlalt"); }
    QString bd   (void) { return this->value("@bd");    }
    QString bdalt(void) { return this->value("@bdalt"); }

    QString grey(void)     { return this->value("@grey"); }
    QString red(void)      { return this->value("@red"); }
    QString orange(void)   { return this->value("@orange"); }
    QString green(void)    { return this->value("@green"); }
    QString teal(void)     { return this->value("@teal"); }
    QString yellow(void)   { return this->value("@yellow"); }
    QString blue(void)     { return this->value("@blue"); }
    QString darkblue(void) { return this->value("@darkblue"); }
    QString magenta(void)  { return this->value("@magenta"); }
    QString violet(void)   { return this->value("@violet"); }
    QString cyan(void)     { return this->value("@cyan"); }
    QString darkcyan(void) { return this->value("@darkcyan"); }

    bool dark(void);
};

//
// dtkThemesEngine.h ends here
