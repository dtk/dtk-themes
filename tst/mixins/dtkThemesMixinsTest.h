#pragma once

#include <QtCore>

class dtkThemesMixinsTestCase : public QObject
{
Q_OBJECT

public:
//    dtkThemesMixinsTest(void);
//    ~dtkThemesMixinsTest(void);

//private slots:
//    void initTestCase(void);
//    void init(void);

private slots:
    void testMixins(void);

//private slots:
//    void cleanupTestCase(void);
//    void cleanup(void);

private:
    class dtkLogCategoryTestCasePrivate *d;
};

//
// dtkLogCategoryTest.h ends here
