#include "dtkThemesMixinsTest.h"

#include <dtkThemesTest>
#include <dtkThemesParser>

void dtkThemesMixinsTestCase::testMixins(void)
{
    bool allStylesCorrect = true;
    QDir rootMixins(":dtk-themes/mixins/");
    qDebug() << "Mixins list: " << rootMixins.entryList();
    for (auto style = rootMixins.entryInfoList().constBegin();
         style != rootMixins.entryInfoList().constEnd(); ++style)
    {
        bool styleCorrect = true;
        QStringList faultyEntries = {};
        dtkThemesParser themeParser;
        themeParser.parse(style->filePath());
        QStringList entries = {"@base0", "@base1", "@base2", "@base3", "@base4", "@base5", "@base6", "@base7", "@base8",
                               "@grey", "@red", "@orange", "@green", "@teal", "@yellow", "@blue", "@darkblue",
                               "@magenta", "@violet", "@cyan", "@darkcyan",
                               "@bg", "@bgalt", "@fg", "@fgalt", "@bd", "@bdalt", "@hl", "@hlalt"
        };
        for (auto entry = entries.constBegin(); entry != entries.constEnd(); ++entry)
        {
            QColor color = themeParser.color(*entry);
            if (!color.isValid())
            {
                styleCorrect = false;
                faultyEntries << *entry;
            }
        }
        if (!styleCorrect)
        {
            qDebug() << "In style " << style->baseName() << " contains errors for: " << faultyEntries;
        }
        allStylesCorrect &= styleCorrect;
    }
    QVERIFY2(allStylesCorrect, "All styles are parsable.");
}
DTKTHEMESTEST_MAIN_NOGUI(dtkThemesMixinsTest, dtkThemesMixinsTestCase);
